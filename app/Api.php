<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    public $api_key;
    public $client_name;
    protected $client_id;
    protected $client;

    public function __construct()
    {
    	//$this->api_key     = "vTRlipLWPikUj7oWUNOg5MaEG8QbppU8";
        $this->api_key     = "aeyWwVoxxS9DxTLvJ0W6scIauKj3Bpbg";
        $this->client_name = "concepthausmx";
        $this->client_id   = "91891325";
        $this->client      = new \GuzzleHttp\Client();
    }

    public function home($page = '1')
    {
        try {
            $result = $this->client->request(
                "GET", 
                "https://www.behance.net/v2/users/concepthausmx/projects",
                [
                    'query' => [
                        'api_key' => $this->api_key, 
                        'page'    => $page, 
                        'fields'   => 'academia'
                    ]
            ]);

            $result = json_decode($result->getBody()->getContents());

            return $result->http_code == "200" ? $result : die();
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return [];
        }
    }

    public function getProjects()
    {
        $x = session()->has('projects') ? null :  session()->put('projects', collect());

        if (session()->get('projects')->isEmpty()) 
        {
            $projects = collect();
            $page     = 1;

            while($result = $this->home($page)->projects)
            {
                $projects->push($result);
                $page++;
            }

            $res = $projects->collapse();

            session()->put('projects', $res);

            return session()->get('projects');
        }

        return session()->get('projects');
    }


    public function getProject($id)
    {
        try {
            $result = $this->client->request(
                "GET", 
                "https://www.behance.net/v2/projects/" . $id,
                [
                    'headers' => [
                        
                    ], 
                    'query' => [
                        'api_key' => $this->api_key
                    ]
            ]);

            $result = json_decode($result->getBody()->getContents());

            return $result->http_code == "200" ? $result->project : die();
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return [];
        }
    }


    public function getUser()
    {
        try {
            $result = $this->client->request(
                "GET", 
                "https://www.behance.net/v2/users/" . $this->client_name,
                [
                    'headers' => [
                        
                    ], 
                    'query' => [
                        'api_key' => $this->api_key
                    ]
            ]);

            $result = json_decode($result->getBody()->getContents());

            return $result->http_code == "200" ? $result->user : die();
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return [];
        }
    }


    public function getUserStats()
    {
        try {
            $result = $this->client->request(
                "GET", 
                "https://www.behance.net/v2/users/" . $this->client_name . "/stats",
                [
                    'headers' => [
                        
                    ], 
                    'query' => [
                        'api_key' => $this->api_key
                    ]
            ]);

            $result = json_decode($result->getBody()->getContents());

            return $result->http_code == "200" ? $result->stats : die();
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return [];
        }
    }
}
