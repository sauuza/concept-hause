<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class AppController extends Controller
{
	protected $api;

	public function __construct()
	{
		$this->api = new Api();
	}

    public function home()
    {
        session()->put('chunk', 0);

    	$projects = $this->api->getProjects()->chunk(15)[session()->get('chunk')]->shuffle();

    	return view('home', compact('projects'));
    }

    public function branding()
    {
        session()->put('chunk', 0);

        $projects = $this->api->getProjects()->chunk(60)[session()->get('chunk')]->shuffle();

        return view('branding', compact('projects'));
    }

    public function web()
    {
        session()->put('chunk', 0);

        $projects = $this->api->getProjects()->chunk(60)[session()->get('chunk')]->shuffle();

        return view('web', compact('projects'));
    }


    public function digital()
    {
        session()->put('chunk', 0);

        $projects = $this->api->getProjects()->chunk(60)[session()->get('chunk')]->shuffle();

        return view('digital', compact('projects'));
    }

    public function audiovisual()
    {
        session()->put('chunk', 0);

        $projects = $this->api->getProjects()->chunk(60)[session()->get('chunk')]->shuffle();

        return view('audiovisual', compact('projects'));
    }

    public function rse()
    {
        session()->put('chunk', 0);

        $projects = $this->api->getProjects()->chunk(200)[session()->get('chunk')]->shuffle();

        return view('rse', compact('projects'));
    }

    public function getProjects(Request $request)
    {
        $data = $request->validate(['filter' => 'nullable']);

        session()->put('chunk', session()->get('chunk') + 1);

        $projects = $this->api->getProjects()->chunk(60)[session()->get('chunk')]->shuffle();
        
        $filters  = $data['filter'] ? explode(',', $data['filter']) : [];

        return view('projects', compact('projects', 'filters'));
    }
}
