<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm;

class ContactController extends Controller
{
    public function __invoke(Request $request)
    {
    	$data = $this->validator($request);
        
        Mail::to('contacto@concepthaus.mx')
            ->bcc('leslye@concepthaus.mx')
            ->send(new ContactForm(collect($data)));

    	return redirect()->back()
            ->with('alert', 'El mensaje se ha enviado con éxito, gracias por contactarnos.');
    }

    public function validator($request)
    {
    	$rules = [
            'name'       => 'required|string|min:3|max:16', 
            'email'      => 'required|string|email|min:5|max:60', 
            'phone'      => 'required|numeric|min:10', 
            //'services'   => 'required|array', 
            //'services.*' => 'required|in:branding,web,digital,audiovisual,rse', 
            'comment'    => 'required|string|max:1024', 
        ];

    	return $request->validate($rules);
    }
}
