<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class ProjectController extends Controller
{
	protected $api;

	public function __construct()
	{
		$this->api = new Api();
	}

    public function show($id)
    {
    	//$data    = $this->validator($request);
        $projects = $this->api->getProjects();
    	$project  = $this->api->getProject($id);

    	return view('project', compact('project', 'projects'));
    }

    public function validator($request)
    {
    	return $request->validate([
    		'id' => 'required|integer', 
    	]);
    }
}
