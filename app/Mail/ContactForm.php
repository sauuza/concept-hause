<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    private $name;
    private $email;
    private $phone;
    private $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name    = $data->name;
        $this->email   = $data->email;
        $this->phone   = $data->phone;
        $this->comment = $data->comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@concepthaus.mx')
            ->subject('Nuevo Cliente Potencial en ConceptHaus')
            ->view('emails.contact')
            ->with([
                'name'    => $this->name, 
                'email'   => $this->email, 
                'phone'   => $this->phone, 
                'comment' => $this->comment, 
            ]);
    }
}
