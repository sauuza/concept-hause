<?php

return [
	'menu' => [
		'contact' => 'Contact'
	], 
    'pages' => [
		'home' => [
			'text' => 'We are a creative cluster specialized in brand creation, development and strengthening through our three branches. <strong>ConceptHaus: branding, advertising, marketing and interior design agency</strong>; InHaus Films: <strong>AV production</strong>; TreeHaus: <strong>Green Marketing strategy and CSR consultancy</strong>.',
		],
		'branding' => [
			'text' => '
				<p>Area specialized in the conceptualization, brand creation and development of corporate identities. Having worked in <strong>the creation of more than 200 brands in the market</strong>, it has a <strong>multidisciplinary team of Graphic Designers</strong>, experts in Branding, directed first hand by the CEO of ConceptHaus Creative Cluster.</p>

				<p class="font-weight-bold mt-5">
					Naming <br />
					Branding <br />
					Corporate Identity <br />
					Brand redesign <br />
					Brandbook <br />
					Conceptual interior design
				</p>
			', 
		], 
		'web' => [
			'text' => '
				<p><strong>Specialized in IT solutions</strong>, this area has an <strong>expert team in UX/UI and web and app development</strong>. From a landing page to a digital platform, we find the most optimal way to give life to projects <strong>always following the highest standards of technology and information design</strong>.</p>

				<p class="font-weight-bold mt-5">
					Design System <br />
					UX/UI Design <br />
					Web development <br />
					App development <br />
					Landing pages <br />
					E-commerce <br />
					Customized software
				</p>
			', 
		], 
		'digital' => [
			'text' => '
				<p>Divided into two main branches, <strong>DigitalHaus has a team specialized in Content Production</strong> that works hand in hand with Media Planning to achieve high levels of engagement by solidifying the DNA of each brand. <strong>It also has a particular expertise in performance campaigns focused on generating conversions through social media and search engines</strong>.</p> 

				<p class="font-weight-bold mt-5">
					Digital Marketing Strategy <br />
					Communication Manual <br />
					Social Networking Content <br />
					Media Advisory <br />
					Conversion campaigns <br />
					SEO and SEM
				</p>
			', 
		], 
		'audiovisual' => [
			'text' => '
				<p>By having a <strong>team with a strong cinematographic background</strong>, this area has achieved great results working with various brands and transnational networks. It specializes in <strong>the creation and execution of Branded Content campaigns that allow brands to achieve organic awareness in the minds of consumers</strong>.</p>

				<p class="font-weight-bold mt-5">
					Creative Conceptualization <br />
					Audiovisual Production <br />
					Post-production <br />
					Still photography <br />
					Product Photography <br />
					Branded content <br />
					360º Videos
				</p>
			', 
		], 
		'rse' => [
			'text' => '
				<p>The sustainable side of the Cluster. <strong>TreeHaus has an expert team in Sustainability and Corporate Social Responsibility</strong> guiding brands towards an environmentally and socially friendly transformation. In addition, it <strong>works together with the different areas of the Cluster to give life to projects with a social or environmental core</strong>.</p>

				<p class="font-weight-bold mt-5">
					Corporate Social Responsibility <br />
					Social Marketing <br />
					Environmental Marketing
				</p>
			', 
		],   
	    'contact' => [
	    	'title' => 'Lets make <span class="theme-color">contact</span>', 
	    	'form'  => [
	    		'name'    => 'Name', 
	    		'email'   => 'E-mail', 
	    		'phone'   => 'Phone', 
	    		'message' => 'I would like to know more about', 
	    		'button'  => 'Send', 
	    	], 
	    	'job' => 'If you are looking for a job or are a supplier, please write to us at '
	    ],  
    ], 
    'header' => [
    	'left' => 'Lets keep in touch'
    ], 
    'footer' => [
    	'privacy'   => 'Notice of privacy', 
    	'copyright' => 'All rights reserved'
    ], 
    'buttons' => [
    	'projects' => [
    		'more' => 'More projects', 
    		'other' => 'Other projects	'
    	], 
    	'contact'  => 'Contact us', 
    	'loading'  => 'Loading ...'
    ], 
    'text' => [
    	'more' => '<strong>More than 500 brands have trusted us</strong>, Lets team up and take your idea away!', 
    ], 
];