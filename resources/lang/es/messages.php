<?php

return [
	'menu' => [
		'contact' => 'Contacto'
	], 
    'pages' => [
		'home' => [
			'text' => 'Somos un clúster creativo especializado en la conceptualización, desarrollo y fortalecimiento de marca compuesto por tres empresas hermanas. ConceptHaus: <strong class="font-weight-bold">agencia de branding, publicidad, marketing e interiorismo</strong>, InHaus Films: <strong class="font-weight-bold">casa productora</strong> y TreeHaus: consultoría de responsabilidad social y empresarial <strong class="font-weight-bold">a favor de la sustentabilidad</strong>.',
		],
		'branding' => [
			'text' => '
				<p class="text-xs-center">Área del Cluster especializada en la conceptualización creación de marca y desarrollo de identidades corporativas.<br />Habiendo trabajado en <strong>la creación de más de 200 marcas vigentes en el mercado</strong>, cuenta con un <strong>equipo multidisciplinario de Diseñadores Gráficos</strong> expertos en Branding, dirigidos de primera mano por el CEO de ConceptHaus Creative Cluster.</p>

				<p class="font-weight-bold mt-5 text-xs-center">
					Naming <br />
					Creación de marca <br />
					Identidad corporativa <br />
					Rediseño de marca <br />
					Brandbook <br />
					Interiorismo conceptual 
				</p>
			', 
		], 
		'web' => [
			'text' => '
				<p class="text-xs-center"><strong>Especializados en soluciones de IT</strong>, esta área del Cluster cuenta con <strong>un equipo experto en UI/UX y desarrollo web y app</strong>. Desde una landing page hasta una plataforma digital, encontramos la forma más óptima de darle vida a los proyectos <strong>respetando siempre los más altos estándares de tecnología y diseño de la información</strong>.</p>

				<p class="font-weight-bold mt-5 text-xs-center">
					Design System <br />
					Diseño UX/UI <br />
					Desarrollo web <br />
					Desarrollo de aplicaciones <br />
					Landing pages <br />
					E-commerce <br />
					Software a la medida 
				</p>
			', 
		], 
		'digital' => [
			'text' => '
				<p class="text-xs-center">Dividida en dos ramas principalmente, <strong>DigitalHaus cuenta con un equipo especializado en Content Production</strong> que trabaja de la mano de Media Planning para lograr aumentar niveles de engagement solidificando el ADN de cada marca. Asimismo, <strong>cuenta con un expertise particular en campañas de performance enfocadas a generar conversiones a través de Redes Sociales y buscadores</strong>.</p> 

				<p class="font-weight-bold mt-5 text-xs-center">
					Estrategia de Marketing Digital <br />
					Manual de voz y tono <br />
					Contenido de Redes Sociales <br />
					Pauta en medios <br />
					Campañas de conversión <br />
					SEO y SEM
				</p>
			', 
		], 
		'audiovisual' => [
			'text' => '
				<p class="text-xs-center">Al contar con <strong>equipo y talento de calidad cinematográfica</strong>, está área del Cluster ha logrado grandes resultados trabajando con PyMES y Networks trasnacionales por igual. Se especializa en <strong>la creación y ejecución de campañas de Branded Content que llevan a las marcas a posicionarse de forma orgánica y visual en la mente de los consumidores</strong>.</p>

				<p class="font-weight-bold mt-5 text-xs-center">
					Conceptualización creativa <br />
					Producción audiovisual <br />
					Postproducción <br />
					Fotografía fija <br />
					Fotografía de producto <br />
					Branded content <br />
					Videos 360 
				</p>
			', 
		], 
		'rse' => [
			'text' => '
				<p class="text-xs-center">El lado sustentable del Cluster. <strong>TreeHaus cuenta con un equipo experto en Sustentabilidad y Responsabilidad Social Empresarial</strong> guiando a las marcas hacia una transformación favorable para el medio ambiente y la sociedad. Además, <strong>trabaja en conjunto con las diversas áreas del Cluster para darle vida a proyectos con un tuétano social o ambiental</strong>.</p>

				<p class="font-weight-bold mt-5 text-xs-center">
					Responsabilidad social empresarial <br />
					Marketing social <br />
					Marketing ambiental 
				</p>
			', 
		],
	    'contact' => [
	    	'title' => 'Hagamos <span class="theme-color">contacto</span>', 
	    	'form'  => [
	    		'name'    => 'Nombre', 
	    		'email'   => 'Correo electrónico', 
	    		'phone'   => 'Teléfono (10 Dígitos)', 
	    		'message' => 'Quisiera saber más sobre', 
	    		'button'  => 'Enviar', 
	    	], 
	    	'job' => 'Si buscas trabajo o eres proveedor, por favor escríbenos a '
	    ],  
    ], 
    'header' => [
    	'left' => 'Mantengamos contacto'
    ], 
    'footer' => [
    	'privacy'   => 'Aviso de privacidad', 
    	'copyright' => 'Derechos reservados'
    ], 
    'buttons' => [
    	'projects' => [
    		'more' => 'Más proyectos', 
    		'other' => 'Otros proyectos'
    	], 
    	'contact'  => 'Contáctanos', 
    	'loading'  => 'Cargando ...'
    ], 
    'text' => [
    	'more' => '<strong>Más de 500 marcas han confiado en nosotros</strong>, ¡Hagamos equipo y llevemos lejos tu idea!', 
    ], 
];
