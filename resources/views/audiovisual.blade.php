@extends('layouts.app')

@php
    $categories = ['Cinematography', 'Advertising Photography', 'Film', 'Production', 'Storyboarding', 'Storytelling', 'Television', 'Photography'];
@endphp

@section('title', 'Concept Haus - InHausFilms. + POST')

@section('seo')
    <link rel="canonical" href="{{ route('home') }}">
@endsection

@section('content')
    <div id="header">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-md-6 align-self-end">
                    <div class="container p-0" style="max-width: 580px;">
                        <h3 class="title my-4 text-xs-center">
                            <img src="{{ asset('assets/img/inhausfilms.svg') }}">
                        </h3>
                        @lang('messages.pages.audiovisual.text')
                    </div>
                </div>
                @foreach ($projects as $key => $project)

                    @foreach($project->fields as $field)
                        @php $x = true; @endphp

                        @if(in_array($field, $categories))
                            <div class="col-md-6" 
                                style="background: url({{ $project->covers->original }}) no-repeat right center; -webkit-background-size: contain; -moz-background-size: contain; -o-background-size: contain; background-size: contain; border-right: 5px solid white;">
                                <a href="{{ route('project', $project->id) }}" class="d-block w-100 h-100"></a>
                            </div>

                            @php $x = false @endphp
                            @php $projects->shift(); @endphp
                            @break;
                        @endif

                        @break(!$x)
                    @endforeach

                    @break(!$x)
                @endforeach
            </div>                
        </div>

        <div id="api-secondary" class="grid">
            @foreach ($projects as $project)
                @foreach($project->fields as $field)
                    @php $x = true; @endphp

                    @if(in_array($field, $categories))
                        <div class="grid-item">
                            <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                                class="smoothLink" 
                                data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                                title="{{ $project->name }}">
                                <img src="{{ $project->covers->original }}" class="img-fluid">
                            </a>
                        </div>

                        @php $x = false @endphp
                        @break;

                        @break(!$x)
                    @endif
                @endforeach
            @endforeach
        </div>

        <button type="button" 
            class="btn btn-lg btn-outline-dark mx-auto d-block text-uppercase btn-house rounded-0 load-more my-5" 
            style="max-width: 350px;" data-filter="{{ implode(',', $categories) }}">
            @lang('messages.buttons.projects.more')
        </button>
    </div>

    <hr />

    @include('fragments.brands')

    @include('fragments.contact-footer')
    
@endsection