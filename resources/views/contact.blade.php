@extends('layouts.app')

@section('title', 'Concept Haus - Contáctanos')

@section('seo')
    <link rel="canonical" href="{{ route('contact') }}">
@endsection

@section('content')
    <div id="header" class="border-bottom pb-5">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title my-4 text-center">@lang('messages.pages.contact.title')</h3>
                    <img src="{{ asset('assets/img/contact.png') }}" class="mx-auto d-block">
                </div>
                <div class="col-md-4">

                    {{ Form::open(['route' => 'contact.post']) }}

                        @include('forms.contact')

                        <button type="" 
                            class="btn btn-outline-dark mx-auto btn-block text-uppercase btn-house rounded-0 px-5 py-2 mt-5 font-weight-bold" style="max-width: 303px;">
                            @lang('messages.pages.contact.form.button')
                        </button>


                        @if (session('alert'))
                            <div id="success" class="alert alert-dismissible animated fadeIn text-success mt-5 mb-3 border border-success" role="alert" style="background-color: white;">
                                <i class="far fa-check-circle"></i> {!! session('alert') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <p class="my-5 text-center">
                            @lang('messages.pages.contact.job') <br /> <a href="mailto:jobs@concepthaus.mx" class="btn btn-link text-danger font-weight-bold">jobs@concepthaus.mx</a>
                        </p>

                    {{ Form::close() }}
                </div>
            </div>                
        </div>
    </div>
@endsection