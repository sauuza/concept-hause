<div class="form-row mt-4">
    <div class="form-group col-md-12">
        <div class="input-group">
            {{ Form::text('name', null, ['placeholder' => trans('messages.pages.contact.form.name') . '*', 'class' => 'form-control' . ($errors->first('name') ? ' is-invalid' : ''), 'autofocus']) }}
        </div>
        @if ($errors->has('name'))
            <span class="small text-muted text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="form-group col-md-12">
        <div class="input-group">
            {{ Form::text('email', null, ['placeholder' => trans('messages.pages.contact.form.email') . '*', 'class' => 'form-control' . ($errors->first('email') ? ' is-invalid' : '')]) }}
        </div>
        @if ($errors->has('email'))
            <span class="small text-muted text-danger">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="form-group col-md-12">
        <div class="input-group">
            {{ Form::text('phone', null, ['placeholder' => trans('messages.pages.contact.form.phone') . '*', 'class' => 'form-control' . ($errors->first('phone') ? ' is-invalid' : '')]) }}
        </div>
        @if ($errors->has('phone'))
            <span class="small text-muted text-danger">{{ $errors->first('phone') }}</span>
        @endif
    </div>
    <div class="form-group col-md-12">
        <div class="input-group">
            {{ Form::textarea('comment', null, ['placeholder' => trans('messages.pages.contact.form.message') . '*', 'class' => 'form-control' . ($errors->first('comment') ? ' is-invalid' : ''), 'rows' => 4]) }}
        </div>
        @if ($errors->has('comment'))
            <span class="small text-muted text-danger">{{ $errors->first('comment') }}</span>
        @endif
    </div>
</div>