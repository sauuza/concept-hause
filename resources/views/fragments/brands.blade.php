@section('css')
    <link rel="stylesheet" href="{{ asset('assets/vendors/splide-2.4.21/dist/css/splide.min.css') }}">
@endsection


@section('js')
    <script src="{{ asset('assets/vendors/splide-2.4.21/dist/js/splide.min.js') }}"></script>

    <script>
        var splide = new Splide( '.splide', {
            type        : 'loop',
            autoWidth   : true,
            autoHeight  : true, 
            focus       : 'center',
            type        : 'loop',
            perPage     : 3,
            autoplay    : true,
            pauseOnHover: false,
            pagination  : false, 
            fixedHeight : '8rem',
        });

        splide.mount();
    </script>
@endsection

<div class="container d-block">
    <div class="row">
        <div class="col">
            <div class="splide">
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/continental.svg') }}" class="mx-auto d-block w-100">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/metlife.svg') }}" class="mx-auto d-block w-100">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/g-be-grand.svg') }}" class="mx-auto d-block w-100">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/fox.svg') }}" class="mx-auto d-block w-100">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/the-walking-dead.svg') }}" class="mx-auto d-block w-100">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/citibanamex.svg') }}" class="d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/discovery.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/national-geographic.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/totalplay.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/loma-linda.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/chilim-balam.svg') }}" class="d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/roberts.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/make-a-wish.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide">
                            <img src="{{ asset('assets/img/brands/world-vision.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/de-alba-fundacion.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/quickbooks.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/macmillan.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/grupo-jv.svg') }}" class="mx-auto d-block">
                        </li>
                        <li class="splide__slide px-5">
                            <img src="{{ asset('assets/img/brands/vibox.svg') }}" class="mx-auto d-block">
                        </li>
                    </ul>
                </div>
                <div class="splide__progress">
                    <div class="splide__progress__bar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<div id="brands" class="d-none d-md-block mt-5">
    <div class="container pb-3">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('assets/img/brands/main.png') }}" class="img-fluid w-100 mx-auto">
            </div>
        </div>
    </div>
</div>-->