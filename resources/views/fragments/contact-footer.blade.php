<div class="border-bottom">
    <div class="container">
        <div class="row my-0 my-md-5 py-5">
            <div class="col-md-12">
                <h5 class="title">@lang('messages.text.more')</h5>

                <a href="{{ route('contact') }}">
                    <img src="{{ asset('assets/img/contact.png') }}" class="mx-auto d-block py-5">
                </a>

                <a href="{{ route('contact') }}" class="btn btn-lg btn-outline-dark mx-auto d-block text-uppercase btn-house rounded-0" style="max-width: 303px;">
                    @lang('messages.buttons.contact')
                </a>
            </div>
        </div>

        <a href="#top" class="top text-center">
            <img src="{{ asset('assets/img/up.svg') }}" class="mx-auto d-block mt-5 my-5">
        </a>
    </div>
</div>