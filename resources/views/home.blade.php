@extends('layouts.app')

@section('title', 'Concept Haus - CREATIVE CLUSTER')

@section('seo')
    <link rel="canonical" href="{{ route('home') }}">
@endsection

@section('content')
    <div id="header">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-md-6 align-self-end">
                    <div class="container p-0" style="max-width: 580px;">
                        <h3 class="title mt-5 mt-md-0 my-4 text-xs-center">
                            Accenting <span class="theme-color">Everything</span>
                        </h3>
                        <p class="text-xs-center">@lang('messages.pages.home.text')</p>
                    </div>

                    <div class="row row-eq-height mt-5">
                        @if ($project = $projects->shift())
                            <div class="col-12 col-md-6 p-1 grid-item">
                                <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                                    class="smoothLink" 
                                    data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                                    title="{{ $project->name }}">
                                    <img src="{{ $project->covers->max_808 }}" class="img-fluid">
                                </a>
                            </div>
                        @endif
                        @if ($project = $projects->shift())
                            <div class="col-12 col-md-6 p-1 grid-item">
                                <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                                    class="smoothLink" 
                                    data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                                    title="{{ $project->name }}">
                                    <img src="{{ $project->covers->max_808 }}" class="img-fluid">
                                </a>
                            </div>
                        @endif
                    </div>
                </div>

                @if ($project = $projects->shift())
                    <div class="col-md-6 project-background" 
                        style="background: url({{ $project->covers->original }}) no-repeat center center;">
                        <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                            class="d-block w-100 h-100 smoothLink" 
                            data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                            title="{{ $project->name }}">
                                
                        </a>
                    </div>
                @endif
            </div>                
        </div>

        <div id="api-secondary" class="grid">
            @foreach ($projects as $key => $project)
                <div class="grid-item">
                    <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                        class="smoothLink" 
                        data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                        title="{{ $project->name }}">
                        <img src="{{ $project->covers->original }}" class="img-fluid">
                    </a>
                </div>
            @endforeach
        </div>

        <button type="button" 
            class="btn btn-lg btn-outline-dark mx-auto d-block text-uppercase btn-house rounded-0 load-more my-5" 
            style="max-width: 350px;" data-filter="">
            @lang('messages.buttons.projects.more')
        </button>
    </div>

    <hr />

    @include('fragments.brands')

    @include('fragments.contact-footer')
    
@endsection