<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Agencia de publicidad, Branding y Marketing digital / Polanco"/>
    <meta name="author" content="FAS Digital, Pyme Web">
    <meta name="generator" content="">
    <title>@yield('title')</title>
    @yield('seo')
    <!-- Facebook -->
    <meta property="og:locale" content="en_US">
    <meta property="og:title" content="Agencia de publicidad, Branding y Marketing digital / Polanco"/>
    <meta property="og:image" content="https://concepthaus.mx/img/image-meta.png" />
    <meta property="og:description" content="Somos expertos en la creación, desarrollo y fortalecimiento de marca. Más de 200 marcas creadas a nivel global. / Polanco"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://concepthaus.mx/" />
    <meta property="og:site_name" content="Concept Haus" />
    <!-- Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Somos expertos en la creación, desarrollo y fortalecimiento de marca. Más de 200 marcas creadas a nivel global."/>
    <meta name="twitter:title" content="Agencia de publicidad, Branding y Marketing digital / Polanco"/>
    <meta name="twitter:domain" content="Concept Haus" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/img/505a70555259730446f01d5590f59bf6.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-4.5.3-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullscreen-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app-custom.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>

    @yield('css')

    <script>
        $(window).on("load", function() {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                //columnWidth: 350
                percentPosition: true, 
                horizontalOrder: false
            });

            setInterval(function(){
                $('.grid').masonry('layout');
            }, 1000);
        });
    </script>
</head>
<body>
    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <a href="{{ route('branding') }}" class="{{ Route::is(['branding']) ? 'active' : null }}" 
                data-url="{{ route('branding') }}">
                Branding
            </a>
            <a href="{{ route('web') }}"  class="{{ Route::is(['web']) ? 'active' : null }}" 
                data-url="{{ route('web') }}">
                Web
            </a>
            <a href="{{ route('digital') }}"  class="{{ Route::is(['digital']) ? 'active' : null }}" 
                data-url="{{ route('digital') }}">
                Digital
            </a>
            <a href="{{ route('audiovisual') }}"  class="{{ Route::is(['audiovisual']) ? 'active' : null }}" 
                data-url="{{ route('audiovisual') }}">
                Audiovisual
            </a>
            <a href="{{ route('rse') }}"  class="{{ Route::is(['rse']) ? 'active' : null }}" 
                data-url="{{ route('rse') }}">
                RSE
            </a>
            <a href="{{ route('contact') }}"  class="{{ Route::is(['contact']) ? 'active' : null }}" 
                data-url="{{ route('contact') }}">
                @lang('messages.menu.contact')
            </a>
            <a href="http://concepthause.fasdigital.online/blog">
                Blog
            </a>
        </div>
    </div>

    <header>
        <div class="container">
            <div class="row h-100">
                <div class="col-3 col-md-3 my-auto pl-1 d-none d-md-block">
                    <span class="title mb-2">
                        @lang('messages.header.left')
                    </span>
                    <a href="//www.instagram.com/concepthausmx/" class="d-inline-block mr-0 mr-md-2"
                        target="_blank">
                        <img src="{{ asset('assets/img/social/instagram.svg') }}">
                    </a>
                    <a href="//www.facebook.com/ConceptHausBranding/" class="d-inline-block mr-1 mr-md-2"
                        target="_blank">
                        <img src="{{ asset('assets/img/social/facebook.svg') }}">
                    </a>
                    <a href="//www.behance.net/concepthausmx" class="d-inline-block mr-2"
                        target="_blank">
                        <img src="{{ asset('assets/img/social/be.svg') }}">
                    </a>
                </div>
                <div class="col-12 col-md-6 text-center">
                    <a href="{{ route('home') }}" data-url="{{ route('home') }}" class="smoothLink animate__animated animate__slideInDown">
                        <img src="{{ asset('assets/img/concept-haus-logo-blanco.svg') }}" class="d-inline-block m-auto img-fluid">
                    </a>

                    <span class="d-inline d-md-none" style="font-size:30px;cursor:pointer; font-size: 30px; cursor: pointer; color: white; float: right;" onclick="openNav()">&#9776;</span>
                </div>
                <div class="col-3 col-md-3 text-right pl-0 d-none d-md-block">
                    <span id="locale" class="title mb-2  mt-3">
                        <a href="{{ route('locale', 'es') }}" 
                            class="smoothLink text-white {{ session()->get('locale') == 'es' ? 'active' : null }}"  
                            data-url="{{ route('locale', 'es') }}">
                            ESP
                        </a> 
                        / 
                        <a href="{{ route('locale', 'en') }}" 
                            class="smoothLink text-white {{ session()->get('locale') == 'en' ? 'active' : null }}" 
                            data-url="{{ route('locale', 'en') }}">
                            ENG
                        </a>
                    </span>
                    <span class="title mb-2 d-none d-md-block">
                        Campos elíseos / CDMX
                    </span>
                </div>
            </div>        
        </div>
    </header>

    <nav>
       <ul class="d-none d-md-flex nav justify-content-center nav-menu py-5">
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['branding']) ? 'active' : null }}" href="{{ route('branding') }}" data-url="{{ route('branding') }}">Branding</a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['web']) ? 'active' : null }}" href="{{ route('web') }}" data-url="{{ route('web') }}">Web</a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['digital']) ? 'active' : null }}" href="{{ route('digital') }}" data-url="{{ route('digital') }}">Digital</a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['audiovisual']) ? 'active' : null }}" href="{{ route('audiovisual') }}" data-url="{{ route('audiovisual') }}">Audiovisual</a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['rse']) ? 'active' : null }}" href="{{ route('rse') }}" data-url="{{ route('rse') }}">RSE</a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link smoothLink {{ Route::is(['contact']) ? 'active' : null }}" href="{{ route('contact') }}" data-url="{{ route('contact') }}">
                    @lang('messages.menu.contact')
                </a>
            </li>
            <li class="nav-item d-none d-md-inline">
                <a class="nav-link" href="http://concepthause.fasdigital.online/blog">Blog</a>
            </li>
        </ul>

        <div class="d-block d-md-none mt-3">
            <span class="float-left ml-3">
                <a href="//www.instagram.com/concepthausmx/" class="d-inline-block mr-2 mr-md-2"
                    target="_blank">
                    <img src="{{ asset('assets/img/social/instagram-black.svg') }}" style="width: 16px;">
                </a>
                <a href="//www.facebook.com/ConceptHausBranding/" class="d-inline-block mr-2 mr-md-2"
                    target="_blank">
                    <img src="{{ asset('assets/img/social/facebook-black.svg') }}" style="width: 7px;">
                </a>
                <a href="//www.behance.net/concepthausmx" class="d-inline-block mr-2"
                    target="_blank">
                    <img src="{{ asset('assets/img/social/be-black.svg') }}" style="width: 17px;">
                </a>            
            </span>

            <span class="float-right mr-3">
                <span id="locale" class="title mb-2  mt-3">
                    <a href="{{ route('locale', 'es') }}" 
                        class="smoothLink text-concept {{ session()->get('locale') == 'es' ? 'active' : null }}" 
                        data-url="{{ route('locale', 'es') }}">
                        ESP
                    </a> 
                    / 
                    <a href="{{ route('locale', 'en') }}" 
                        class="smoothLink text-concept {{ session()->get('locale') == 'en' ? 'active' : null }}" 
                        data-url="{{ route('locale', 'en') }}">
                        ENG
                    </a>
                </span>         
            </span>
        </div>
    </nav>

    <main>
        @yield('content')
    </main>

    <footer>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-3 my-auto">
                    <p>TEL.: <a href="tel:5552820707" class="btn btn-link text-muted">55 5282 0707</a> <br /> <a href="mailto:contacto@concepthaus.mx" class="btn btn-link text-muted p-0">contacto@concepthaus.mx</a></p>
                </div>
                <div class="col-12 col-md-6 my-auto">
                    <a href="#top"><img src="{{ asset('assets/img/concept-haus-logo.svg') }}" class="mx-auto d-block"></a>
                    <p class="text-center mt-4 small">Concept Haus / @lang('messages.footer.copyright')  2020</p>
                </div>
                <div class="col-12 col-md-3 my-auto">
                    <p>
                        <a href="{{ route('privacy') }}" class="text-privacy btn btn-link text-muted">
                            @lang('messages.footer.privacy')
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    
    <script src="{{ asset('assets/js/lazyload.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/fullscreen-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app-custom.js') }}"></script>

    <script>
        var lazyLoadInstance = new LazyLoad( { elements_selector: ".lazy" } );

        if ( lazyLoadInstance ) lazyLoadInstance.update()


        $( '.load-more' ).click( function() {
            $.ajaxSetup( { headers: { 'X-CSRF-TOKEN' : $( 'meta[name="csrf-token"]' ).attr( 'content' ) } });

            var filter = $( this ).attr( "data-filter" );

            $.ajax( {
                async: true,
                url:   "{{ route('app.projects.get') }}",
                type:  "POST", 
                data:  { "filter" : filter }, 

                beforeSend: function( xhr ) {
                    $( '.load-more' ).html( '@lang("messages.buttons.loading")' ).prop( "disabled", true );

                }, 

                success: function( result ) {
                    var $elems = $( result );
                    
                    $( '.grid' ).append( $elems ).masonry( 'appended', $elems );
                }, 

                complete: function() {
                    $( '.load-more' ).html( '@lang("messages.buttons.projects.more")' ).prop( "disabled", false );

                }
            } );
        } );

        $( "a[href='#top']" ).click( function() {
            $( "html, body" ).animate( { scrollTop: 0 }, "slow" );

            return false;
        } );
        
        $( "a.smoothLink" ).click( function(e) {
            $( "body" ).fadeOut( function() {
                window.location = e.currentTarget.attributes['data-url'].value;
            } );
        } );

    </script>

    @yield('js')
</body>
</html>