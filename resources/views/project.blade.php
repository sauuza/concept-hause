@extends('layouts.app')

@section('title', 'Concept Haus - ' . $project->name)

@section('seo')
    <link rel="canonical" href="{{ route('project', $project->id) }}/{{ $project->slug }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/vendors/splide-2.4.21/dist/css/splide.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/splide-2.4.21/dist/js/splide.min.js') }}"></script>

    <script>
        var splide = new Splide( '.splide', {
            type   : 'loop',
            perPage: 3,
            breakpoints: {
                640: {
                    perPage: 1,
                },
            }, 
            autoplay    : true,
            pauseOnHover: false,
            pagination: true, 
        });

        splide.mount();
    </script>
@endsection

@section('content')
    <div id="header">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h3 class="title my-4 text-center text-uppercase">
                        {{ $project->name }}
                    </h3>
                    <h4 class="text-center text-uppercase" style="font-size: 18px;">
                        @foreach ($project->fields as $key => $field)
                            {{ $field }} {{ ($key + 1 < count($project->fields)) ? '/' : null }}
                        @endforeach
                    </h4>
                    <p class="text-center">{{ $project->description }}</p>
                </div>
            </div>                
        </div>
    </div>

    <div id="api" class="container py-5">
        <div class="row">
            <div class="col-md-12">
                @foreach ($project->modules as $key => $module)
                    <div class="item shadow mb-5 d-block">
                        @switch($module->type)
                            @case('image')
                                <img data-src="{{ $module->sizes->original }}" class="img-fluid w-100 mx-auto d-block lazy animate__animated animate__fadeIn">
                            @break

                            @case('video')
                                <div class="embed-responsive embed-responsive-16by9" 
                                    style="background-color: black;">
                                    {!! $module->embed !!}
                                </div>                        
                            @break

                            @case('text')
                                
                            @break

                            @case('embed')
                                
                            @break

                        @default
                            
                        @endswitch
                    </div>
                @endforeach
            </div>
            <div class="col-md-12">
                <h3 class="title my-5 text-center text-uppercase">
                    @lang('messages.buttons.projects.other')
                </h3>

                <div class="container">
                    @if ($categories = $project->fields)
                        <div class="splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    @php $x = 0 @endphp

                                    @foreach ($projects->shuffle() as $key => $project)

                                        @foreach ($project->fields as $field)
                                            @if (in_array($field, $categories))

                                                <li class="splide__slide">
                                                    <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                                                        class="smoothLink d-block" 
                                                        data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                                                        title="{{ $project->name }}"
                                                        style="overflow: hidden;">
                                                        <img src="{{ $project->covers->max_808 }}" class="img-fluid">
                                                    </a>

                                                    <h4 class="text-center text-uppercase mt-3 mb-5" style="font-size: 18px;">
                                                        {{ $project->name }}
                                                    </h4>
                                                </li>

                                                @php $x++ @endphp

                                                @break

                                            @endif

                                        @endforeach

                                        @break ($x > 5)

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>

                <br />

                <a href="#top" class="">
                    <img src="{{ asset('assets/img/up.svg') }}" class="mx-auto d-block mt-5 my-5">
                </a>
            </div>
        </div>
    </div>

    <hr />
@endsection