@foreach ($projects as $project)

    @if($categories = $filters)

        @foreach($project->fields as $field)
            @php $x = true; @endphp

            @if(in_array($field, $categories))
                <div class="grid-item">
                    <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                        class="smoothLink" 
                        data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                        title="{{ $project->name }}">
                        <img src="{{ $project->covers->original }}" class="img-fluid">
                    </a>
                </div>

                @php $x = false @endphp
                @break;

                @break(!$x)
            @endif
        @endforeach
        
    @else
        <div class="grid-item">
            <a href="{{ route('project', $project->id) }}/{{ $project->slug }}" 
                class="smoothLink" 
                data-url="{{ route('project', $project->id) }}/{{ $project->slug }}"
                title="{{ $project->name }}">
                <img src="{{ $project->covers->original }}" class="img-fluid">
            </a>
        </div>
    @endif

@endforeach
