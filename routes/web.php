<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
})->name('home');*/

Route::get('/', 'AppController@home')
    ->name('home')
    ->middleware('locale');


Route::post('/', 'AppController@getProjects')
    ->name('app.projects.get');


Route::get('locale/{locale?}', function ($locale = 'es') {
    session()->put('locale', $locale);

    return redirect()->back();
})->name('locale');


Route::get('/proyecto/{id}/{slug?}', 'ProjectController@show')
	->name('project')
    ->middleware('locale');
	

Route::get('/branding', 'AppController@branding')
    ->name('branding')
    ->middleware('locale');


Route::get('/web', 'AppController@web')
    ->name('web')
    ->middleware('locale');


Route::get('/digital', 'AppController@digital')
    ->name('digital')
    ->middleware('locale');


Route::get('/audiovisual', 'AppController@audiovisual')
    ->name('audiovisual')
    ->middleware('locale');


Route::get('/rse', 'AppController@rse')
    ->name('rse')
    ->middleware('locale');


Route::get('/contacto', function () {
    return view('contact');
})
    ->name('contact')
    ->middleware('locale');


Route::post('/contacto', 'ContactController')
	->name('contact.post');
    

Route::get('/aviso-de-privacidad', function () {
    return view('privacy');
})
    ->name('privacy')
    ->middleware('locale');